# lualatex-5e-sheet

## What is this repository?

This is a Character Sheet template for Dungeons & Dragons 5th Edition written in LuaLaTeX.<br>
It supports automatic calculation of things such as proficiency bonus, skill modifiers, equipment weight and more.

## How do I use it?

First, you need to install [LuaTeX](http://luatex.org/) and [DND-5e-LaTeX-Template](https://github.com/rpgtex/DND-5e-LaTeX-Template).<br>
Once you have done that, make a copy of the template file, edit it and then run `lualatex <your file name>`.

## License

Content inside the examples is licensed under [Open Gaming License](https://gitlab.com/cyanx9/lualatex-5e-sheet/-/blob/main/OGL), everything else is under [GNU GPL](https://gitlab.com/cyanx9/lualatex-5e-sheet/-/blob/main/LICENSE).